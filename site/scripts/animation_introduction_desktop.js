const customEvent = new Event('introFinished');

$(document).ready(function () {
  var animation_introduction_desktop = bodymovin.loadAnimation({
    container: document.getElementById('animation_introduction_desktop'),
    renderer: 'svg',
    loop: false,
    autoplay: true,
    path: './json/home_introduction_desktop.json',
  });

  animation_introduction_desktop.onComplete = function () {
    displayContent();
    SetIndexAnimNotVisible();
  }

  // Event Listener
  $("#passerAnim").on("click", stopAnim);

  /**
   * Fonction qui stop l'animation et display les bonnes div
   */
  function stopAnim() {
    animation_introduction_desktop.stop();
    displayContent();
    SetIndexAnimNotVisible();
  }

  function displayContent() {
    var eltIn = document.getElementById("index-anim");
    eltIn.classList.remove("is-visible");
    var eltOut = document.getElementById("index-content");
    eltOut.classList.add("is-visible");
    document.dispatchEvent(customEvent);
  }

  if (sessionStorage.isIndexAnimVisible == undefined) {
    sessionStorage.setItem('isIndexAnimVisible', "true");
  } else if (sessionStorage.isIndexAnimVisible === "false") {
    displayContent();
  }

  /**
   * Fonction qui change d'état à True animation
   */
  function SetIndexAnimVisible() {
    sessionStorage.isIndexAnimVisible = "true";
  }
  /**
   * Fonction qui change d'état à False animation
   */
  function SetIndexAnimNotVisible() {
    sessionStorage.isIndexAnimVisible = "false";
  }
});