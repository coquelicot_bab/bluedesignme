const classBarCookieVisible = "cookie-not-visible";

// !!! ---- Portée des variables ---- !!! 
$(document).ready(function () {
  // Si la variable d'affichage des cookies n'est pas défini -> De base affiché 
  if (sessionStorage.isCookiesVisible == undefined) {
    // Variable globale qui défini l'affichage des cookies
    sessionStorage.setItem('isCookiesVisible', "true");
  } else if (sessionStorage.isCookiesVisible === "false") {
    hideCookiebar();
  }

  // Event Listener
  $("#agree").on("click", hideCookiebar);

  /**
   * Fonction qui cache la bar d'utilisation des Cookies
   */
  function hideCookiebar() {
    $("#cookie-bar").addClass(classBarCookieVisible);
    $("#agree").addClass(classBarCookieVisible);
    SetCookiesNotVisible();
  }

  /**
   * Fonction qui change d'état à True la visibilitée des Cookies
   */
  function SetCookiesVisible() {
    sessionStorage.isCookiesVisible = "true";
  }
  /**
   * Fonction qui change d'état à False la visibilitée des Cookies
   */
  function SetCookiesNotVisible() {
    sessionStorage.isCookiesVisible = "false";
  }
});