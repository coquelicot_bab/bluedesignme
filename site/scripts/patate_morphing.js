$(document).ready(function () {
  var animation_patate_morphing = bodymovin.loadAnimation({
    container: document.getElementById('animation_patate_morphing'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_patate_morphing.json',
  });
});
