const langFr = "fr"; // Constante ne peut jamais changer
const langEn = "en";
const langEs = "es";

$(document).ready(function () {
  // Si la langue n'est pas défini -> De base la langue sera le FR
  if (localStorage.lang == undefined) {
    // Variable globale qui défini la langue active 
    localStorage.setItem('lang', langFr);
  }

  // On appel la fonction qui change la langue du site
  setLanguage();

  // On ajoute un écouteur d'évènements à chaque bouton pour changer de langue
  $("#lang-switch .flag-fr").on("click", { language: langFr }, changeLanguage);
  $("#lang-switch .flag-en").on("click", { language: langEn }, changeLanguage);
  $("#lang-switch .flag-es").on("click", { language: langEs }, changeLanguage);

  /**
   * Fonction qui set la langue active avec la langue passée en paramètre
   * @param {*} language 
   */
  function changeLanguage(event) {
    localStorage.lang = event.data.language;

    // On appel la fonction qui change la langue du site
    setLanguage();
  }

  /**
   * Fonction qui change la langue du site par rapport a la valeur de la variable global "lang"
   */
  function setLanguage() {
    // On récupère la balise body
    var baliseBody = $('body,html');
    // On enlève par retirer toutes les classes de la balise body
    baliseBody.removeClass();

    // on ajoute la classe de la langue active au body
    baliseBody.addClass(`active-lang-${localStorage.lang}`);
  }
});

$(document).ready(function () {
  var choose_language_fr = bodymovin.loadAnimation({
    container: document.getElementById('choose_language_fr'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/language_fr.json',
  });

  var choose_language_en = bodymovin.loadAnimation({
    container: document.getElementById('choose_language_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/language_en.json',
  });

  var choose_language_es = bodymovin.loadAnimation({
    container: document.getElementById('choose_language_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/language_es.json',
  });

});
