$(document).ready(function () {
  var logo_contact_me_out = document.getElementById('logo_contact_me_out')
  var animationOut = bodymovin.loadAnimation({
    container: logo_contact_me_out,
    renderer: "svg",
    loop: false,
    autoplay: false,
    prerender: true,
    path: './json/contactez-moi-out.json',
  });

  var logo_contact_me_container = document.getElementById('logo_contact_me_container')

  logo_contact_me_container.addEventListener("mouseenter", function () {
    animationOut.stop();
  });

  logo_contact_me_container.addEventListener("mouseleave", function () {
    animationOut.play();
  });

  animationOut.onComplete = function () {
    var eltIn = document.getElementById("logo_contact_me_in");
    eltIn.classList.toggle("hidden");
    var eltOut = document.getElementById("logo_contact_me_out");
    eltOut.classList.toggle("hidden");
  }
});

