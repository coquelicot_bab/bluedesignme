var animShow = "anim_show";
var fadeIn = "fade-in";
var fadeOut = "fade-out";
$(document).ready(function () {

  /*********** 
   * Lotties 
   ***********/
  var home_animation_description = bodymovin.loadAnimation({
    container: document.getElementById('home_animation_description'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_animation_description.json',
  });

  var anim_dsc_txt_head_lab = document.getElementById('anim_dsc_txt_head_lab')
  var animationHead = bodymovin.loadAnimation({
    container: anim_dsc_txt_head_lab,
    renderer: "svg",
    loop: true,
    autoplay: true,
    prerender: true,
    path: './json/home_head_lab.json',
  });


  /*********** 
   * Scroll
   ***********/
  var scrollY = function () {
    var supportPageOffset = window.pageXOffset !== undefined;
    var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");
    return supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
  }
  var offset = 40;
  var classFixed = 'fixed';
  var containerElt = document.getElementById('container_animation_description');
  var rectCtnElt = containerElt.getBoundingClientRect();
  var topCtnElt = 0;
  var bottomCtnElt = 0;
  var elt = document.getElementById('home_animation_description_txt');
  var rectElt = elt.getBoundingClientRect();
  var topElt = rectElt.top + scrollY();
  var bottomElt = rectElt.bottom + scrollY();

  var onScroll = function () {
    if (scrollY() > bottomCtnElt && bottomCtnElt <= 0) {
      elt.style.top = offset + "px";
      elt.style.bottom = "auto";
    } else if (scrollY() > bottomCtnElt) {
      elt.style.top = "auto";
      elt.style.bottom = offset + "px";
    } else if (topCtnElt < scrollY()) {
      elt.style.top = offset + scrollY() - topCtnElt + "px";
      elt.style.bottom = "auto";
    } else if (scrollY() < topCtnElt) {
      elt.style.top = offset + "px";
      elt.style.bottom = "auto";
    }
  }

  var recalculteAll = function () {
    calculateElt();
    calculateCtnElt();
    onScroll();
  }

  var calculateCtnElt = function () {
    rectCtnElt = containerElt.getBoundingClientRect();
    topCtnElt = rectCtnElt.top + scrollY();
    bottomCtnElt = topCtnElt + rectCtnElt.height - rectElt.height - offset;
  }

  var calculateElt = function () {
    rectElt = elt.getBoundingClientRect();
    topElt = rectElt.top + scrollY();
    bottomElt = rectElt.bottom + scrollY();
  }

  document.addEventListener('scroll', onScroll);

  home_animation_description.addEventListener('DOMLoaded', function (e) {
    recalculteAll();
  });
  document.addEventListener('introFinished', function (e) {
    recalculteAll();
  }, false);


  /***************
   * Transitions
   ***************/
  var buttonStartAnim = document.getElementById('btn-starts-anim');
  var buttonChangeAnim = document.getElementById('btn-change-anim');
  var anim_dsc_anim_0 = document.getElementById('anim_dsc_anim_0');
  var anim_dsc_anim_1 = document.getElementById('anim_dsc_anim_1');
  var index = 1;
  var indexMax = 5;
  buttonStartAnim.addEventListener("click", function () {
    anim_dsc_anim_0.classList.add(fadeOut);
    recalculteAll();
  });
  anim_dsc_anim_0.addEventListener('animationend', () => {
    if (anim_dsc_anim_0.className.includes(fadeOut)) {
      anim_dsc_anim_0.classList.remove(animShow);
      anim_dsc_anim_0.classList.remove(fadeIn);
      anim_dsc_anim_0.classList.remove(fadeOut);
      anim_dsc_anim_1.classList.add(animShow);
      anim_dsc_anim_1.classList.add(fadeIn);
    }
    if (anim_dsc_anim_0.className.includes(fadeIn)) {
      var dscTxtToHide = document.getElementById(`dsc_txt_${indexMax}`);
      var dscTxtToShow = document.getElementById(`dsc_txt_1`);
      dscTxtToHide.classList.remove(animShow);
      dscTxtToHide.classList.remove(fadeIn);
      dscTxtToHide.classList.remove(fadeOut);
      dscTxtToShow.classList.add(animShow);
    }
    recalculteAll();
  });

  anim_dsc_anim_1.addEventListener('animationend', () => {
    if (anim_dsc_anim_1.className.includes(fadeOut)) {
      anim_dsc_anim_1.classList.remove(animShow);
      anim_dsc_anim_1.classList.remove(fadeIn);
      anim_dsc_anim_1.classList.remove(fadeOut);
      anim_dsc_anim_0.classList.add(animShow);
      anim_dsc_anim_0.classList.add(fadeIn);
    }
    recalculteAll();
  });

  buttonChangeAnim.addEventListener("click", function () {
    if (index < indexMax) {
      var dscTxtToHide = document.getElementById(`dsc_txt_${index}`);
      var dscTxtToShow = document.getElementById(`dsc_txt_${index + 1}`);

      dscTxtToHide.classList.add(fadeOut);

      dscTxtToHide.addEventListener('animationend', () => {
        if (dscTxtToHide.className.includes(fadeOut)) {
          dscTxtToHide.classList.remove(fadeIn);
          dscTxtToHide.classList.remove(animShow);
          dscTxtToHide.classList.remove(fadeOut);
          dscTxtToShow.classList.add(animShow);
          dscTxtToShow.classList.add(fadeIn);
        }
      });
      index = index + 1;
    } else {
      anim_dsc_anim_1.classList.add(fadeOut);
      index = 1;
    }
    recalculteAll();
  });
});