$(document).ready(function () {
  var logo_identite_visu = bodymovin.loadAnimation({
    container: document.getElementById('logo_identite_visu'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_identite_visu.json',
  });

  var logo_identite_visu_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_identite_visu_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_identite_visu_eng.json',
  });

  var logo_identite_visu_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_identite_visu_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_identite_visu_esp.json',
  });
});