/*function createAudio(assetPath) {
  console.log("toto")
  return new Howl({
      src: [assetPath]
  })}
  */
$(document).ready(function () {

  var logo_communication = bodymovin.loadAnimation({
    container: document.getElementById('logo_communication'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_communication.json',
    /*audioFactory: createAudio,*/
  });

  var logo_communication_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_communication_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_communication_eng.json',
    /*audioFactory: createAudio,*/
  });

  var logo_communication_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_communication_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_communication_esp.json',
    /*audioFactory: createAudio,*/
  });

  var sound = new Howl({
    src: ['./../music/AR02timeless.mp3']
  });

  logo_communication.DOMloaded = function () {
    console.log("toto")
    var context = new AudioContext();
    context.resume();
    sound.play();
  }

});
