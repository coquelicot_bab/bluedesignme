$(document).ready(function () {
  var logo_wordpress = bodymovin.loadAnimation({
    container: document.getElementById('logo_wordpress'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_wordpress.json',
  });

  var logo_wordpress_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_wordpress_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_wordpress_eng.json',
  });

  var logo_wordpress_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_wordpress_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_wordpress_esp.json',
  });
});

