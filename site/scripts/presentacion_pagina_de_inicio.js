$(document).ready(function () {
  var logo_motion_presentation = bodymovin.loadAnimation({
    container: document.getElementById('logo_presentation'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_presentacion_pagina_de_inicio.json',
  });
});
