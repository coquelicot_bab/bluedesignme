$(document).ready(function () {
  var menu_accueil_identite_visu = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_identite_visu'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_identite_visu.json',
  });

  var menu_accueil_identite_visu_burger = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_identite_visu_burger'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_identite_visu_burger.json',
  });

  var menu_accueil_wordpress = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_wordpress'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_wordpress.json',
  });

  var menu_accueil_wordpress_burger = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_wordpress_burger'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_wordpress_burger.json',
  });

  var menu_accueil_motion_design = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_motion_design'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_animation.json',
  });

  var menu_accueil_motion_design_burger = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_motion_design_burger'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_animation_burger.json',
  });

  var menu_accueil_montage_video = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_montage_video'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_montage_video.json',
  });

  var menu_accueil_montage_video_burger = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_montage_video_burger'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_montage_video_burger.json',
  });

  var menu_accueil_web = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_web'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_web.json',
  });

  var menu_accueil_web_burger = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_web_burger'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_web_burger.json',
  });

  var menu_accueil_communication = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_communication'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_communication.json',
  });

  var menu_accueil_communication_burger = bodymovin.loadAnimation({
    container: document.getElementById('menu_accueil_communication_burger'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/home_page_menu_communication_burger.json',
  });
});

