$(document).ready(function () {
  var logo_montage = bodymovin.loadAnimation({
    container: document.getElementById('logo_montage'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_montage.json',
  });

  var logo_montage_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_montage_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_montage_eng.json',
  });

  var logo_montage_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_montage_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_montage_esp.json',
  });

});
