$(document).ready(function () {
  var above_footer = bodymovin.loadAnimation({
    container: document.getElementById('above_footer'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/above_footer.json',
  });
});