$(document).ready(function () {
  var logo_motion_presentation = bodymovin.loadAnimation({
    container: document.getElementById('logo_presentation'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_presentation.json',
  });
});

$(document).ready(function () {
  var logo_motion_presentation_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_presentation_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_presentacion_pagina_de_inicio.json',
  });
});

$(document).ready(function () {
  var logo_motion_presentation_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_presentation_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_presentation_home.json',
  });
});
