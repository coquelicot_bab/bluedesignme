$(document).ready(function () {
  var logo_contact_me_in = document.getElementById('logo_contact_me_in')
  var animationIn = bodymovin.loadAnimation({
    container: logo_contact_me_in,
    renderer: "svg",
    loop: false,
    autoplay: false,
    prerender: true,
    path: './json/contactez-moi.json',
  });

  var logo_contact_me_container = document.getElementById('logo_contact_me_container')

  logo_contact_me_container.addEventListener("mouseenter", function () {
    animationIn.play();
  });

  logo_contact_me_container.addEventListener("mouseleave", function () {
    animationIn.stop();
  });

  animationIn.onComplete = function () {
    var eltIn = document.getElementById("logo_contact_me_in");
    eltIn.classList.toggle("hidden");
    var eltOut = document.getElementById("logo_contact_me_out");
    eltOut.classList.toggle("hidden");
  }
});