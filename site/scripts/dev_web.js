$(document).ready(function () {
  var logo_dev_web = bodymovin.loadAnimation({
    container: document.getElementById('logo_dev_web'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_dev_web.json',
  });

  var logo_dev_web_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_dev_web_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_dev_web_eng.json',
  });

  var logo_dev_web_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_dev_web_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_dev_web_esp.json',
  });
});