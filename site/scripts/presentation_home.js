$(document).ready(function () {
  var logo_motion_presentation = bodymovin.loadAnimation({
    container: document.getElementById('logo_presentation'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_presentation_home.json',
  });
});
