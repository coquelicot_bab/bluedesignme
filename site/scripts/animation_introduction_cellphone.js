$(document).ready(function () {
  var animation_introduction_cellphone = bodymovin.loadAnimation({
    container: document.getElementById('animation_introduction_cellphone'),
    renderer: 'svg',
    loop: false,
    autoplay: true,
    path: './json/home_introduction_cellphone.json',
  });

  animation_introduction_cellphone.onComplete = function () {
    displayContent();
  }

  // Event Listener
  $("#passerAnim").on("click", stopAnim);

  /**
   * Fonction qui stop l'animation et display les bonnes div
   */
  function stopAnim() {
    animation_introduction_cellphone.stop();
    displayContent();
  }

  function displayContent() {
    var eltIn = document.getElementById("index-anim");
    eltIn.classList.remove("is-visible");
    var eltOut = document.getElementById("index-content");
    eltOut.classList.add("is-visible");
  }
});
