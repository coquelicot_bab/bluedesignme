$(document).ready(function () {
  var logo_motion_design_fr = bodymovin.loadAnimation({
    container: document.getElementById('logo_motion_design_fr'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_motion_design.json',
  });

  var logo_motion_design_en = bodymovin.loadAnimation({
    container: document.getElementById('logo_motion_design_en'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_motion_design.json',
  });

  var logo_motion_design_es = bodymovin.loadAnimation({
    container: document.getElementById('logo_motion_design_es'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './json/logo_motion_design_esp.json',
  });


});
